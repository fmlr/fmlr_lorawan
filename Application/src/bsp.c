/******************************************************************************
 * @file    bsp.c
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   manages the sensors on the application
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "bsp.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Exported functions ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
SHT_HandleTypedef h_sht;

/** Array of sensors */
static sensor_t sensors[MAX_SENSORS];

static I2C_HandleTypeDef h_I2C;

static void I2C_Init()
{
  h_I2C.Instance = I2C1;
  h_I2C.Init.ClockSpeed = 100000;
  h_I2C.Init.DutyCycle = I2C_DUTYCYCLE_2;
  h_I2C.Init.OwnAddress1 = 0;
  h_I2C.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  h_I2C.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  h_I2C.Init.OwnAddress2 = 0;
  h_I2C.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  h_I2C.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&h_I2C) != HAL_OK) {
    Error_Handler();
  }
}

void HAL_I2C_MspInit(I2C_HandleTypeDef* i2cHandle)
{
  GPIO_InitTypeDef GPIO_InitStruct;
  if (i2cHandle->Instance == I2C1) {
    /**I2C1 GPIO Configuration
    PB8     ------> I2C1_SCL
    PB9     ------> I2C1_SDA
    */
    GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();
  }
}

const sensor_t* BSP_sensor_Read()
{
  for (uint8_t i = 0; i < MAX_SENSORS; i++) {
    switch (sensors[i].type) {
    case eNoSensor:
      return sensors;
    case eSHT21:
      sensors[i].data.humidity.t = (SHTGetTemp((SHT_HandleTypedef*)(sensors[i].private)) + 5) / 10;
      sensors[i].data.humidity.rh = SHTGetHumidity((SHT_HandleTypedef*)(sensors[i].private));
      break;
    case eSTS21:
      sensors[i].data.temperature.t = (SHTGetTemp((SHT_HandleTypedef*)(sensors[i].private)) + 5) / 10;
      break;
    default:
      break;
    }
  }
  return sensors;
}

/**
 * @brief  initializes the sensor
 *
 * @param sensorConfig  sensor configuration data
 * @retval Number of sensors found or -1 in case of error
 */
int8_t  BSP_sensor_Init(const volatile eSensorConfig_t* sensorConfig)
{
  // Initialize I2C
  I2C_Init();

  uint8_t sensorCnt = 0;

  if (sensorConfig->reportBattLevel == ENABLE) {
    PRINTF("Battery level reporting enabled!\n\r");
  }

  // check all possible sensors
  eSensorType_t sht = SHTInitSensor(&h_sht, &h_I2C);
  if (sht != eNoSensor) {
    if (sensorCnt >= MAX_SENSORS) { goto to_many; }
    sensors[sensorCnt].type = sht;
    sensors[sensorCnt++].private = &h_sht;
    PRINTF("Sensor '%s' detected!\n\r", sht == eSHT21 ? "SHT21" : "STS21");
  }

  return sensorCnt;

to_many:
  PRINTF("Too many sensors defined!\n\r");
  return -1;
}
