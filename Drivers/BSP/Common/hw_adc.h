/******************************************************************************
 * @file    hw_msp.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   Header for driver hw msp module
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __HW_ADC_H__
#define __HW_ADC_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

/*!
 * \brief Get the current temperature from internal temperature sensor
 *
 * \retval value  temperature in 0.1 �C
 */
uint16_t HW_GetTemperatureLevel(void);

/*!
 * \brief Get the current battery level
 *
 * \retval value  battery level ( 0: very low, 254: fully charged )
 */
uint8_t HW_GetBatteryLevel(void);

/**
  * @brief Read voltage on ADC pin
  *
  * Read ADC value from port and compensate with calibrated VREF
  *
  * @param ADC channel
  * @retval Voltage on port in [mV]
  */
float HW_GetVoltage(uint32_t channel);

/*!
 * \brief Initializes the ADC input
 *
 * \param [IN] scl  ADC input pin name to be used
 */
void HW_AdcInit(void);

/*!
 * \brief DeInitializes the ADC
 *
 * \param [IN] none
 */
void HW_AdcDeInit(void);

/*!
 * \brief Read the analogue voltage value
 *
 * \param [IN] Channel to read
 * \retval value    Analogue pin value
 */
uint16_t HW_AdcReadChannel(uint32_t Channel);

#ifdef __cplusplus
}
#endif

#endif /* __HW_ADC_H__ */
