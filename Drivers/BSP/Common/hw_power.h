/*
 ******************************************************************************
 * @file    hw_msp.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   Header for driver hw msp module
 ******************************************************************************
 */
#ifndef __HW_POWER_H__
#define __HW_POWER_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  e_LOW_POWER_RTC = (1 << 0),
  e_LOW_POWER_GPS = (1 << 1),
  e_LOW_POWER_UART = (1 << 2), /* can be used to forbid stop mode in case of uart Xfer*/
  e_LOW_POWER_LED = (1 << 3),
} e_LOW_POWER_State_Id_t;

/** MCU reset source definition */
typedef enum {
  eResetNone,
  eResetWU,
  eResetPIN,
  eResetLPW,
  eResetSW,
  eResetPOR,
  eResetIWDG,
  eResetWWDG
} eResetSrc_t;

/*!
* \brief Initializes the HW and enters stope mode
*/
void HW_EnterStopMode(void);

/*!
* \brief Initializes the HW and enters Standby mode
*/
void HW_EnterStandbyMode(void);

/*!
 * \brief Exits stop mode and Initializes the HW
 */
void HW_ExitStopMode(void);

/**
  * @brief Enters Low Power Sleep Mode
  * @note ARM exists the function when waking up
  * @param none
  * @retval none
  */
void HW_EnterSleepMode(void);

#ifdef __cplusplus
}
#endif

#endif /* __HW_POWER_H__ */
