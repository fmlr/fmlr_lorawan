/**
  ******************************************************************************
  * @file    FMLR.c
  * @author  Alex Raimondi
  * @version V1.0.0
  * @date    17-Dec-2016
  * @brief   This file contains definitions for:
  *          - LEDs available on FMLR
  *          - Flash device on FMLR
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FMLR.h"

GPIO_TypeDef* LED_PORT[LEDn] = {LEDR_GPIO_PORT, LEDG_GPIO_PORT};
const uint16_t LED_PIN[LEDn] = {LEDR_PIN, LEDG_PIN};
const GPIO_PinState LED_OFF_STATE[LEDn] = {LEDR_OFF, LEDG_OFF};
