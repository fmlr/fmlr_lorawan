## Adding Drivers, Middlewares and Scripts to your project

```
git remote add drivers git@gitlab.com:miromicoag/Drivers.git
git remote add middlewares git@gitlab.com:miromicoag/Middlewares.git
git remote add scripts git@gitlab.com:miromicoag/Scripts.git

git subtree add --prefix=Drivers/ drivers master --squash
git subtree add --prefix=Middlewares/ middlewares master --squash
git subtree add --prefix=Scripts/ scripts master --squash
```

After that, you can push normally to your project repo.

## Pulling from subtree remote

```
git subtree pull --prefix Drivers/ drivers master --squash 
git subtree pull --prefix Middlewares/ middlewares master --squash 
git subtree pull --prefix Scripts/ scripts master --squash 
```

## Pulling an older subtree commit

To switch to an older commit of the subtree, you have to delete the subtree and add the older version.

Example:
```
git rm Scripts -r
git commit -m "Change Scripts version"
git subtree add --prefix=Scripts scripts 60756e53a57af50fb5ec98f6279fbd67720c2cbd 
```